docker ps :
List containers

docker run hello-world :
This message shows that your installation appears to be working correctly.

docker run redislabs/redismod :
runs a Docker container with the RedisGraph module from Redis Labs.

docker run phisit11/nginx-nodejs-redis-web1 :
does not find image

docker ps :
see container working

docker run phisit11/nginx-nodejs-redis-web1:0227 :
image founded but container redis doesn't exists

docker run -it phisit11/nginx-nodejs-redis-web1:0227 sh :
runs an interactive Docker container based on the "phisit11/nginx-nodejs-redis-web1:0227" image, starting a shell session
can't ping redis

docker network create docker :
This command creates a Docker network named "docker"

docker run --network docker --hostname redis redislabs/redismod :
set the hostname to "redis", because phisit11/nginx-nodejs-redis-web1:0227 need it

docker run --network docker --hostname web phisit11/nginx-nodejs-redis-web1:0227
it work : Web application is listening on port 5000
ping work

docker run --network docker --hostname redis -p 6379:6379 redislabs/redismod :
exposes the Redis module on port 6379

docker run --network docker --hostname web -p 80:5000 phisit11/nginx-nodejs-redis-web1:0227 :
exposes web module port 80

curl localhost :
get the number of visit

docker run --network docker -v redis_data:/data --hostname redis -p 6379:6379 redislabs/redismod :
now, the data from the container persist

curl localhost:8080
see the number of visit (increase)

docker run -d --network docker -v redis_data:/data --hostname redis -p 6379:6379 redislabs/redismod :
launch in daemon (no trace)

docker run -d --network docker --hostname web -p 8080:5000 phisit11/nginx-nodejs-redis-web1:0227 :
launch in daemon

 docker logs e20f1548518a :
see logs

docker run -it --network docker -v redis_data:/data --hostname ubuntu ubuntu :
launch a container of ubuntu

root@ubuntu:/#  ls /data/
dump.rdb
show file of the container

---------------------------------
# DOCKER BDD / IHM WEB
---------------------------------

docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -d mysql
launch mysql daemon

docker run --name phpmyadmin -d --link mysql:db -p 8080:80 phpmyadmin :
launch phpmyadmin in daemon on port 8080:80

webbrowser on laptop : http://127.0.0.1:8080/
credentials : root | password

---------------------------------
# DOCKER WALLABAG
---------------------------------

docker run --name wallabag-db -e "MYSQL_ROOT_PASSWORD=password" -d mariadb
create a mariadb bdd

docker run --name wallabag --link wallabag-db:wallabag-db -e "MYSQL_ROOT_PASSWORD=password" -e "SYMFONY__ENV__DATABASE_DRIVER=pdo_mysql" -e "SYMFONY__ENV__DATABASE_HOST=wallabag-db" -e "SYMFONY__ENV__DATABASE_PORT=3306" -e "SYMFONY__ENV__DATABASE_NAME=wallabag" -e "SYMFONY__ENV__DATABASE_USER=wallabag" -e "SYMFONY__ENV__DATABASE_PASSWORD=wallapass" -e "SYMFONY__ENV__DATABASE_CHARSET=utf8mb4" -e "SYMFONY__ENV__DOMAIN_NAME=http://localhost" -p 80:80 wallabag/wallabag
start wallabag


---------------------------------
# DOCKER - OPTIONNEL 1
---------------------------------

docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -d mariadb
mysql daemon

docker run --name phpmyadmin -d --link mysql:db -p 8080:80 phpmyadmin
localhost:8080 credentials : root | password

---------------------------------
# DOCKER - OPTIONNEL 2
---------------------------------

docker run --name postgres -e POSTGRES_PASSWORD=password -d postgres
launch postgres daemon

docker pull dpage/pgadmin4:latest
get pgadmin4

docker run .......
