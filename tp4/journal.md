git archive -o latestdate +%Y%m%d_%H%M%S.zip HEAD : 
Create files zip with actual datetime

rm latest20240126_102517.zip :
Remove zip file

git clone --depth 1--branch master repo2 extract : 
Clone repo on branch master into extract

rm extract: 
Remove extract

git show object: 
Show object = object is blobs, trees, tags and commits

git show $REVISION:$FILE : git show main:tp1 :
Show details tp1 on branch main

git show HEAD^^^^:$FILE : git show HEAD^^^^:tp1 : 
Show details files tP1 on current branch

git show HEAD~12:$FILE : git show HEAD~12:tp1 : 
HEAD~12 doesn't exist, show error

