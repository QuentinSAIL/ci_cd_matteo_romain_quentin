git init --bare :
Initialize an empty Git repository, but omit the working directory

git clone repo_git repo2 :
Make the same direcotry as repo_git named repo2

git config --global user.email & git config --global user.name :
save git username and email

git commit -m "Initial empty commit" --allow-empty :
allow a commit without file

git clone ssh://user@localhost:2222/home/user/repo_git :
clone repo_git on laptop from the VM

git log :
see the details of the commit (branch, Author, Date)
